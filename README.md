# vimrc

vimrc generator & bashrc aliases 

So the yaml script for ansible builds necessary lines in both ~/.bashrc & ~/.vimrc files to get a buffer band, a file information bar & a current mode information bar in VIM. It also provides a macro to easily build a TITLE figlet styled text as comments on `@A` call.

![vim : a buffer-band](pics/bufferband.png)

![vim : fileinfo bar](pics/fileinfo.png)

![vim : current mode](pics/modeinfo.png)

For the bashrc script, the scripts generates a list of commands, macros & aliases to be run, the most visible one is the PS1 prompt variables that display last command exit status, history number for recalling & of course the current path itself.

![bash : colored prompt](pics/colored_prompt_informations.png)

# How to fix the escape sequence common issue

I use the lineifile module from ansible interpreter but I had an issue on that from the escape sequences management. The solution is to follow the [Learn yaml in minutes](https://learnxinyminutes.com/docs/yaml/) link.

Here is how the escape sequences can be set in the yaml file as over-use quotes to fix that : 

```
    - name : few aliases & shortcuts for .bashrc
      lineinfile:
        path: "{{ bashrc }}"
        line: '{{ item.line }}'
        state: present
      with_items:
        - { line : 'export PATH=$PATH:/snap/bin' }
        - { line : 'docker run francoispussault/fortunes:latest' }
        - { line : 'alias df="df -h -text4"' }
        - { line : 'alias pgbackup="pg_dumpall | gzip > ~/Documents/postgresql.old/pg_dump_postgres.$(date +%Y%m%d).gz && pg_dump -Fc -d francois > ~/Documents/postgresql.old/pg_francois_postgres.$(date +%Y%m%d)"' }
        - { line : 'alias laptopsession="/home/francois/GITLAB/dev/dev_shell_privatework/laptopsession.sh"' }
        - { line : 'alias gitlognice="git log --all --decorate --oneline --graph"' }
        - { line : 'eval "$(thefuck --alias)"' }
        - { line : 'export LESS_TERMCAP_mb=$''\e[1;32m''' }
        - { line : 'export LESS_TERMCAP_md=$''\e[1;32m''' }
        - { line : 'export LESS_TERMCAP_me=$''\e[0m''' }
        - { line : 'export LESS_TERMCAP_se=$''\e[0m''' }
        - { line : 'export LESS_TERMCAP_so=$''\e[01;33m''' }
        - { line : 'export LESS_TERMCAP_ue=$''\e[0m''' }
        - { line : 'export LESS_TERMCAP_us=$''\e[1;4;31m''' }
        - { line : 'export PS1=''\[\e[0;1;91m\]$?\[\e[0m\]-\[\e[0;1;92m\]\!\[\e[0m\]-\[\e[0;1;92m\]\#\[\e[0m\]-\[\e[0;1;91m\]\w \[\e[0;1;92m\]$ \[\e[0m\]''' }
```

then the escape characters are followed & compared properly to the current version of ~/.bashrc file.

# How to run the script for yourself

- Set up your git _(if needed)_

- Git clone this project to a local directory & enter inside it

- Do not forget to edit the script to set those **line :** statement fit your own needs

- Then ensure your own ansible tool box is set properly _(if needed)_ 

- Then finally adapt your own host file to fit your machines IP addresses like below with or not adding user et password depending on your security needs

```
[lesdeux]
127.0.0.1 ansible_ssh_pass=somepassword ansible_ssh_user=francois
```

- Now Run `ANSIBLE_NOCOWS=1 ansible-playbook -i ./hosts vim2vimrc.yaml --ask-become-pass`

```
PLAY [lesdeux] *****************************************************************

TASK [Gathering Facts] *********************************************************
ok: [127.0.0.1]

TASK [Install required packages] ***********************************************
ok: [127.0.0.1]

TASK [Ensure .vim/{autoload,bundle} directory exists] **************************
ok: [127.0.0.1] => (item=/root/.vim)
ok: [127.0.0.1] => (item=/root/.vim/autoload)
ok: [127.0.0.1] => (item=/root/.vim/bundle)

TASK [Ensure Pathogen is in place] *********************************************
ok: [127.0.0.1]

TASK [Deploy plugins] **********************************************************
ok: [127.0.0.1] => (item={'name': 'vim-airline', 'url': 'https://github.com/vim-airline/vim-airline'})
ok: [127.0.0.1] => (item={'name': 'nerdtree', 'url': 'https://github.com/preservim/nerdtree'})
ok: [127.0.0.1] => (item={'name': 'fzf-vim', 'url': 'https://github.com/junegunn/fzf.vim'})
ok: [127.0.0.1] => (item={'name': 'vim-gitgutter', 'url': 'https://github.com/airblade/vim-gitgutter'})
ok: [127.0.0.1] => (item={'name': 'vim-fugitive', 'url': 'https://github.com/tpope/vim-fugitive'})
ok: [127.0.0.1] => (item={'name': 'vim-floaterm', 'url': 'https://github.com/voldikss/vim-floaterm'})

TASK [Ensure .vimrc config in place] *******************************************
ok: [127.0.0.1]

TASK [now few macros & setup line numbers & syntax coloration to vimrc] ********
ok: [127.0.0.1] => (item={'line': 'let @q = ":.!figlet\\<CR>"'})
ok: [127.0.0.1] => (item={'line': 'let @b = ":.,.+5 s/./#&/\\<CR>"'})
ok: [127.0.0.1] => (item={'line': 'let @A = "@qk@b\\<cr>"'})
ok: [127.0.0.1] => (item={'line': 'set nu'})
ok: [127.0.0.1] => (item={'line': 'syntax on'})
ok: [127.0.0.1] => (item={'line': 'let @F = ":! aspell -l fr check %\\<cr>"'})
ok: [127.0.0.1] => (item={'line': 'let @E = ":! aspell -l en check %\\<cr>"'})

TASK [few aliases & shortcuts for .bashrc] *************************************
ok: [127.0.0.1] => (item={'line': 'export PATH=$PATH:/snap/bin'})
ok: [127.0.0.1] => (item={'line': 'docker run francoispussault/fortunes:latest'})
ok: [127.0.0.1] => (item={'line': 'alias df="df -h -text4"'})
ok: [127.0.0.1] => (item={'line': 'alias pgbackup="pg_dumpall | gzip > ~/Documents/postgresql.old/pg_dump_postgres.$(date +%Y%m%d).gz && pg_dump -Fc -d francois > ~/Documents/postgresql.old/pg_francois_postgres.$(date +%Y%m%d)"'})
ok: [127.0.0.1] => (item={'line': 'alias laptopsession="/home/francois/GITLAB/dev/dev_shell_privatework/laptopsession.sh"'})
ok: [127.0.0.1] => (item={'line': 'alias gitlognice="git log --all --decorate --oneline --graph"'})
ok: [127.0.0.1] => (item={'line': 'eval "$(thefuck --alias)"'})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_mb=$'\\e[1;32m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_md=$'\\e[1;32m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_me=$'\\e[0m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_se=$'\\e[0m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_so=$'\\e[01;33m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_ue=$'\\e[0m'"})
ok: [127.0.0.1] => (item={'line': "export LESS_TERMCAP_us=$'\\e[1;4;31m'"})
ok: [127.0.0.1] => (item={'line': "export PS1='\\[\\e[0;1;91m\\]$?\\[\\e[0m\\]-\\[\\e[0;1;92m\\]\\!\\[\\e[0m\\]-\\[\\e[0;1;92m\\]\\#\\[\\e[0m\\]-\\[\\e[0;1;91m\\]\\w \\[\\e[0;1;92m\\]$ \\[\\e[0m\\]'"})
ok: [127.0.0.1] => (item={'line': 'alias meteo="curl wttr.in/~Toulouse"'})
ok: [127.0.0.1] => (item={'line': 'alias catvim=\'vim --cmd "set t_ti= t_te=" +redraw +q\''})
ok: [127.0.0.1] => (item={'line': "alias ll='ls -l --color'"})

PLAY RECAP *********************************************************************
127.0.0.1                  : ok=8    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```
