execute pathogen#infect()
syntax on
filetype plugin indent on

" Configuration vim Airline
set laststatus=2
set tabstop=4

let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1

" Configuration NERDTree
map <F5> :NERDTreeToggle<CR>

" Configuration floaterm
let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_width = 0.9
let g:floaterm_height = 0.9

" Configuration Vim.FZF
let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }
set nu
let @q = ":.!figlet\<CR>"
let @b = ":.,.+5 s/./#&/\<CR>"
let @A = "@qk@b\<cr>"
let @F = ":! aspell -l fr check %\<cr>"
let @E = ":! aspell -l en check %\<cr>"
      
" few shebangs for often used files
:autocmd BufNewFile *.fs 0put=\"#! /usr/bin/env gforth-fast\<nl>\\\<nl>\"|$
:autocmd BufNewFile *.py 0put=\"#! /usr/bin/env python\<nl>\<nl>\"|$
:autocmd BufNewFile *.sh 0put=\"#! /usr/bin/env bash\<nl># -*- coding: UTF8 -*-\<nl>\<nl>\"|$
