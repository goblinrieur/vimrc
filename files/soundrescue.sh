#! /usr/bin/env bash
# -*- coding: UTF8 -*-

 systemctl --user enable wireplumber
 systemctl --user enable pipewire
 systemctl --user restart wireplumber
 systemctl --user restart pipewire
# systemctl --user status wireplumber
# systemctl --user status pipewire
 sudo alsactl store
 systemctl --user disable pulseaudio
 systemctl --user stop pulseaudio
 systemctl --user mask pulseaudio
 amixer -c 0 sset Master unmute
 amixer -c 0 sset Master 100%
# aplay /usr/share/sounds/alsa/Front_Center.wav
