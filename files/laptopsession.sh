#! /usr/bin/env bash

/usr/bin/lxterminal &
/usr/bin/firefox-esr &
/usr/bin/discord &
/usr/games/steam &
/usr/bin/flameshot &
if [ $(uptime -p | awk -F' ' '{print $2}') -lt 15 ] ; then
	sudo ~/GITLAB/dev/dev_shell_privatework/getnmountall.sh
fi

exit 0
